<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
</head>
<body>
    <h1>Register</h1>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <p>
        <label for="first_name">First Name:</label></p>
        <input type="text" name="first_name" required><br>

        <p>
        <label for="last_name">Last Name:</label></p>
        <input type="text" name="last_name" required><br>

        <p>Gender :</p>
        <input type="radio" value="male" checked name="jk">Male<br>
        <input type="radio" value="female" name="jk">Female<br>
        <input type="radio" value="other" name="jniskelamin">Other<br>
            
        <p>Nationality :</p>
        <select name="bangsa">
            <option value="" selected>Indonesia</option>
            <option value="">Inggris</option>
            <option value="">India</option>
            <option value="">Rusia</option>
        </select>

        <p>Language Spoken :</p>
        <input type="checkbox" name="" checked>Bahasa Indonesia<br>
        <input type="checkbox" name="">English<br>
        <input type="checkbox" name="">Other<br>

        <p>Bio :</p>
        <textarea name="textarea" rows="10" cols="40"></textarea><br>

        <button type="submit">Register</button>
    </form>
</body>
</html>
